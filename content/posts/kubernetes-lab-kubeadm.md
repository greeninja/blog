---
title: Kubernetes Lab with Kubeadm
date: 2021-02-18
description: Setup a small Kubernetes lab on CentOS 7 with Kubeadm
tags:
  - Kubernetes
  - Kubeadm
  - Longhorn
  - Labs
author: Nick Campion
toc: true
---

## Introduction

In this lab we will build out a small Kubernetes cluster using kubeadm. This will also include a test Wordpress app and an ingress controller. The lab will consist of a bastion VM, 3 control plane nodes and 3 worker nodes.

{{< notice warning >}}
This lab assumes that libvirt is setup and includes DNS resolution of host names from within the subnet and that Ansible is available
{{< /notice >}}

## Initial Cluster Build

**Prerequisite**

1. Clone the lab repo from GitHub with `git clone --recursive https://github.com/greeninja/kvm-kube-kubeadm-lab.git`
2. Copy the `inventory.yaml.example` file to `inventory.yaml`
3. Set things like the ssh_key and ssh_password in `inventory.yaml`

### Setup VMs

- Run the `setup.yaml` playbook in the `lab-setup/playbooks` directory once the inventory.yaml file has been completed.

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible-playbook -i inventory.yaml lab-setup/playbooks/setup.yaml
{{< /code >}}

- The result should look something like this:

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
Id Name State

---

57 kube-master01.kubernetes.lab running
58 bastion.kubernetes.lab running
59 kube-master02.kubernetes.lab running
60 kube-node01.kubernetes.lab running
61 kube-master03.kubernetes.lab running
62 kube-node02.kubernetes.lab running
63 kube-node03.kubernetes.lab running
103 lb01.kubernetes.lab running
{{< /code >}}

### Verify DNS

- From the bastion (10.44.60.5), ensure that all the other nodes resolve in DNS

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
for i in kube-master{01..03} kube-node{01..03}; do
echo -n "$i => "; dig +short $i;
done
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  kube-master01 => 10.44.60.184
  kube-master02 => 10.44.60.153
  kube-master03 => 10.44.60.161
  kube-node01 => 10.44.60.175
  kube-node02 => 10.44.60.152
  kube-node03 => 10.44.60.185
  {{< /code >}}

## Setup Kubeadm

### Setup the Kubernetes Nodes

With the inventory file that was created to build the lab, the quick start playbook can be run. Alternatively, if the bastion node will be used to run Ansible an inventory file like this will suffice.

## {{< code language="yaml" expand="Show" collapse="Hide" isCollapsed="false" >}}

kube-masters:
hosts:
kube-master01:
ansible_host: 10.44.60.36
kube-master02:
ansible_host: 10.44.60.56
kube-master03:
ansible_host: 10.44.60.28

kube-nodes:
hosts:
kube-node01:
ansible_host: 10.44.60.91
kube-node02:
ansible_host: 10.44.60.55
kube-node03:
ansible_host: 10.44.60.27
{{< /code >}}

{{< notice tip >}}
The quick start Ansible playbook is included in the GitHub repo. This can be run with `ansible-playbook -i inventory.yaml quickstart/setup.yaml`
{{< /notice>}}

> If the quickstart playbook is used, [continue from here](#setup-kubernetes)

#### Setup Ansible on the Bastion node

> If the Bastion node won't be used to run Ansible, these steps can be skipped. [Continue from here](#ansible-is-setup)

- Add EPEL repository

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
yum install -y epel-release
{{< /code >}}

- Install Ansible

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
yum install -y ansible
{{< /code >}}

#### Ansible is Setup

- Using this inventory, install Podman

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible -i inventory.yaml all -m package -a "name=podman state=installed"
{{< /code >}}

- Disable SELinux

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible -i inventory.yaml all -m selinux -a "state=disabled"
{{< /code >}}

- Add the Kubernetes repo to the machines

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible -i inventory.yaml all -m yum_repository -a \
 "name=kubernetes \
 baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch \
 file=kubernetes \
 enabled=yes \
 gpgcheck=yes \
 repo_gpgcheck=yes \
 gpgkey='https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg' \
 description=kube-repo"
{{< /code >}}

- Install `kubelet` `kubeadm` and `kubectl` on the Kubernetes nodes

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible -i inventory.yaml all -m package -a "name=kubeadm,kubelet,kubectl state=installed"
{{< /code >}}

- Start Kubelet

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible -i inventory.yaml all -m service -a "name=kubelet state=started enabled=true"
{{< /code >}}

### Setup the Load Balancer

For this lab, it will be a simple NGINX TCP load balancer.

{{< notice tip >}}
This is setup by the `quickstart/setup.yaml` playbook.
{{< /notice >}}

- Install EPEL

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible -i inventory.yaml load_balancers -m package -a "name=epel-release state=installed"
{{< /code >}}

- Install NGINX

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible -i inventory.yaml load_balancers -m package -a "name=nginx state=installed"
{{< /code >}}

- Setup the `nginx.conf` to act as a proxy to the 3 masters.

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}

# Setup with ansible

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

include /usr/share/nginx/modules/\*.conf;

events {
worker_connections 1024;  
}

stream {
upstream kube_api {
least_conn;
server kube-master01:6443 max_fails=2 fail_timeout=10s;
server kube-master02:6443 max_fails=2 fail_timeout=10s;
server kube-master03:6443 max_fails=2 fail_timeout=10s;
}

server {
listen 6443;
proxy_pass kube_api;
}
}

http {
server {
listen 8080 default*server;
server_name *;
root /usr/share/nginx/html;
include /etc/nginx/default.d/\*.conf;
location / {
}

    location /nginx_status {
      stub_status;
      allow all;
    }

    error_page 404 /404.html;
    location = /40x.html {
    }

}
}
{{< /code >}}

## Setup Kubernetes

### Initial Control Plane

> From the first control plane node `kube-master01`

- Initialise the control plane

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubeadm init \
 --control-plane-endpoint "lb01.kubernetes.lab:6443" \
 --upload-certs \
 --pod-network-cidr=10.244.0.0/16
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of the control-plane node running the following command on each as root:

kubeadm join lb01.kubernetes.lab:6443 --token 1o3clk.vt2g8ix8lnqpfdpe \
 --discovery-token-ca-cert-hash sha256:015ab1bb3c83d23d0810303b40dfeb8e72977129c614290d61425b5af932a452 \
 --control-plane --certificate-key 72102764e59b60d471e5d74244bb279ec17995c486d91261f4864e306aad8b92

Please note that the certificate-key gives access to cluster sensitive data, keep it secret!
As a safeguard, uploaded-certs will be deleted in two hours; If necessary, you can use
"kubeadm init phase upload-certs --upload-certs" to reload certs afterward.

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join lb01.kubernetes.lab:6443 --token 1o3clk.vt2g8ix8lnqpfdpe \
 --discovery-token-ca-cert-hash sha256:015ab1bb3c83d23d0810303b40dfeb8e72977129c614290d61425b5af932a452
{{< /code >}}

{{< notice tip >}}
Keep a note of the join commands as these will be required later to add the remaining nodes in the cluster
{{< /notice >}}

### Bastion

- Set up the Bastion node with the Kube config

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
mkdir ~/.kube
{{< /code >}}

- Copy the kube config from the first control plane node

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
scp kube-master01:/etc/kubernetes/admin.conf .kube/config
{{< /code >}}

- Confirm `kubectl` works from the Bastion

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl get nodes
{{< /code >}}

### Flannel Pod Network

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  NAME STATUS ROLES AGE VERSION
  kube-master01.kubernetes.lab NotReady control-plane,master 15m v1.20.2
  {{< /code >}}

- Install Flannel as the Pod Network

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml
{{< /code >}}

- Wait until all pods are now running

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl get pods --all-namespaces
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  NAMESPACE NAME READY STATUS RESTARTS AGE
  kube-system coredns-74ff55c5b-cnx5x 1/1 Running 0 3m56s
  kube-system coredns-74ff55c5b-k5pkb 1/1 Running 0 3m56s
  kube-system etcd-kube-master01.kubernetes.lab 1/1 Running 0 4m4s
  kube-system kube-apiserver-kube-master01.kubernetes.lab 1/1 Running 0 4m4s
  kube-system kube-controller-manager-kube-master01.kubernetes.lab 1/1 Running 0 4m4s
  kube-system kube-flannel-ds-vctqm 1/1 Running 0 71s
  kube-system kube-proxy-5f86b 1/1 Running 0 3m56s
  kube-system kube-scheduler-kube-master01.kubernetes.lab 1/1 Running 0 4m4s
  {{< /code >}}

### Extra Control Plane Nodes

- Join both the remaining master nodes using the [tokens generated here](#initial-control-plane)

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubeadm join lb01.kubernetes.lab:6443 \
 --token qp81u4.42zph1nkq54xb8iw \
 --discovery-token-ca-cert-hash sha256:3c95d692e25df8167e2c644cbd58aee282b32d4535957ac53daf3b7277984eeb \
 --control-plane --certificate-key 1fe3ad3aa4f18b3a1618279a1408223234472577d911c60f3e6f30a00f65ceff
{{< /code >}}

- Example

{{< asciinema id=392102 >}}

- Example of `kubectl get nodes` after all Control plane nodes have been added

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
NAME STATUS ROLES AGE VERSION
kube-master01.kubernetes.lab Ready control-plane,master 26m v1.20.2
kube-master02.kubernetes.lab Ready control-plane,master 16m v1.20.2
kube-master03.kubernetes.lab Ready control-plane,master 6m31s v1.20.2
{{< /code >}}

### Add Worker Nodes

- Join each of the worker nodes, using the [tokens generated here](#initial-control-plane)

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubeadm join lb01.kubernetes.lab:6443 \
 --token qp81u4.42zph1nkq54xb8iw \
 --discovery-token-ca-cert-hash sha256:3c95d692e25df8167e2c644cbd58aee282b32d4535957ac53daf3b7277984eeb
{{< /code >}}

- The cluster should now have 3 Control plane nodes and 3 worker nodes

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
NAME STATUS ROLES AGE VERSION
kube-master01.kubernetes.lab Ready control-plane,master 30m v1.20.2
kube-master02.kubernetes.lab Ready control-plane,master 20m v1.20.2
kube-master03.kubernetes.lab Ready control-plane,master 11m v1.20.2
kube-node01.kubernetes.lab Ready <none> 91s v1.20.2
kube-node02.kubernetes.lab Ready <none> 64s v1.20.2
kube-node03.kubernetes.lab Ready <none> 47s v1.20.2
{{< /code >}}

## HAProxy Ingress Controller

This lab will be using the [HAProxy ingress controller](https://github.com/haproxytech/kubernetes-ingress#readme).

To deploy HAProxy to the cluster, apply the deploy yaml from GitHub on the Bastion node.

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl apply -f https://gist.githubusercontent.com/greeninja/375b23fb504b8bbfb855347943de553c/raw/f72a3bb2d60d10c93c0dd0a07cfbdd212f3b1ee4/centos-7-haproxy-ingress-controller.yaml
{{< /code >}}

- Check that there are 3 haproxy-ingress pods running

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n haproxy-controller get pods -o wide
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  NAME READY STATUS RESTARTS AGE IP NODE NOMINATED NODE READINESS GATES
  haproxy-ingress-587b77b44f-cbwzc 1/1 Running 0 2m20s 10.44.60.152 kube-node01.kubernetes.lab <none> <none>
  haproxy-ingress-587b77b44f-d5s6k 1/1 Running 0 41s 10.44.60.156 kube-node03.kubernetes.lab <none> <none>
  haproxy-ingress-587b77b44f-hzblt 1/1 Running 0 41s 10.44.60.200 kube-node02.kubernetes.lab <none> <none>
  ingress-default-backend-78f5cc7d4c-8hbbg 1/1 Running 0 2m20s 10.244.5.6 kube-node03.kubernetes.lab <none> <none>
  {{< /code >}}

## Wordpress Test

### Longhorn Storage Class

This section will create an example Wordpress deployment inside the Kubernetes cluster. As Wordpress also requires storage for both the web app and database, we will first deploy a storage service. As it came up in conversation the other day, this will be [Longhorn](https://github.com/longhorn/longhorn)

{{< notice warning >}}
Ensure `iscsi-initiator-utils` is installed on the nodes else this will hang. It is installed as part of the quick start playbook
{{< /notice >}}

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v0.8.0/deploy/longhorn.yaml
{{< /code >}}

- Create an ingress object for the UI

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n longhorn-system apply -f https://raw.githubusercontent.com/greeninja/kvm-kube-kubeadm-lab/master/longhorn-ingress.yaml
{{< /code >}}

- Check that the ingress object has been created

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n longhorn-system get ingress longhorn-ingress
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  NAME CLASS HOSTS ADDRESS PORTS AGE
  longhorn-ingress <none> longhorn.kubernetes.lab 80 90m
  {{< /code >}}

- Providing proxies / DNS is available, the web GUI should now be available at [longhorn.kubernetes.lab](http://longhorn.kubernetes.lab)

{{< figure src="https://s3.devurandom.io/devurandom/img/kube/longhorn.jpg" alt="longhorn GUI" caption="Longhorn GUI" captionPosition="center" style="border-radius: 8px;" >}}

- Check the storage class exists

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl get storageclass
{{< /code >}}

- Example output

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
NAME PROVISIONER RECLAIMPOLICY VOLUMEBINDINGMODE ALLOWVOLUMEEXPANSION AGE
longhorn driver.longhorn.io Delete Immediate true 106m
{{< /code >}}

- Add the storage class to the cluster if it doesn't exist

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl create -f https://raw.githubusercontent.com/longhorn/longhorn/v0.8.0/examples/storageclass.yaml
{{< /code >}}

- Set Longhorn as the default storage class

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl patch storageclass longhorn -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
{{< /code >}}

### Wordpress Deployment

- Create the Wordpress namespace

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl apply -f https://raw.githubusercontent.com/greeninja/kvm-kube-kubeadm-lab/master/wordpress-namespace.yaml
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  namespace/wordpress created
  {{< /code >}}

- Create the database required for Wordpress

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n wordpress apply -f https://raw.githubusercontent.com/greeninja/kvm-kube-kubeadm-lab/master/mysql-deployment.yaml
{{< /code >}}

- Wait until the database pod is Running

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n wordpress get pods
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  NAME READY STATUS RESTARTS AGE
  wordpress-mysql-8c76b9544-dtqbz 1/1 Running 0 64s
  {{< /code >}}

- Create the Wordpress pod

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n wordpress apply -f https://raw.githubusercontent.com/greeninja/kvm-kube-kubeadm-lab/master/wordpress-deployment.yaml
{{< /code >}}

- Wait until the Wordpress pod is Running

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n wordpress get pods
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  NAME READY STATUS RESTARTS AGE
  wordpress-79c9564674-54p98 1/1 Running 0 70s
  wordpress-mysql-8c76b9544-dtqbz 1/1 Running 0 3m5s
  {{< /code >}}

> If you set up the Longhorn GUI, then at this point you should see the two Volumes in use in the dashboard

- Create an ingress controller to Wordpress

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n wordpress apply -f https://raw.githubusercontent.com/greeninja/kvm-kube-kubeadm-lab/master/wordpress-ingress.yaml
{{< /code >}}

- Check the ingress object was created

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
kubectl -n wordpress get ingress
{{< /code >}}

- Example output
  {{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
  NAME CLASS HOSTS ADDRESS PORTS AGE
  wordpress-ingress <none> wordpress.kubernetes.lab 80 39s
  {{< /code >}}

- Providing proxies / DNS is available, Wordpress should be available at [wordpress.kubernetes.lab](http://wordpress.kubernetes.lab)

{{< figure src="https://s3.devurandom.io/devurandom/img/kube/wp-setup.jpg" alt="[WordPress-setup-screen" caption="Wordpress Setup Screen" captionPosition="center" style="border-radius: 8px;" >}}

## Conclusion

At this point there should be a highly available Kubernetes cluster over 3 control-plane nodes, with 3 worker nodes along with a functioning storage class for dynamic provisioning of persistent volume claims and an ingress controller to allow HTTP access into the running applications. For testing purposes there is a Wordpress deployment backed by the storage class to retain data.

### Cleanup

To cleanup this lab there is a tear down playbook in the [GitHub repo](https://github.com/greeninja/kvm-kube-kubeadm-lab)

- To run the cleanup playbook

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
ansible-playbook -i inventory.yaml lab-setup/playbooks/teardown.yaml
{{< /code >}}
