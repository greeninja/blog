---
title: Openshift Cheat Sheet
description: Cheat sheet of all things openshift
date: 2020-08-18
tags:
  - Openshift
---

Get inside the netnamespace of a running container - for use of tools like ping:

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
$ nsenter -u -n -i -p -t $(docker inspect --format "{{.State.Pid}}" <pod1-container-id>)
{{< /code >}}


