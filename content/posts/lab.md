---
title: Source of the Madness
date: "2020-07-21"
tags:
  - Lab
cover: https://s3.devurandom.io/devurandom/img/lab.jpg
---

Is a Dell T5600 workstation courtesy of the guys at [BargainHardware](https://bargainhardware.co.uk) with:

- 2 x Intel Xeon E5-2660 V1 8 Core CPU's
- 8 x 16GB DDR3
- 2 x 2TB Sata Spinners
- 2 x 1TB NVMe
- And a wifi adapter ([this was a pain]({{< ref "/posts/fedora-server-wifi" >}})), running Fedora server 34 and KVM.
