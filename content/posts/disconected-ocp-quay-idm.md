---
title: OCP 4.14 Disconnected with IdM & Standalone Quay
date: 2024-05-18
description: A UPI installation of OpenShift 4.15 using Quay, IdM and kernel arguments to bootstrap CoreOS.
tags:
  - OCP4
  - Openshift
  - Quay
  - IdM
  - Disconnected
  - KVM
  - Labs
author: Nick Campion
toc: true
draft: true
cover: https://s3.devurandom.io/devurandom/img/to-do.jpg
---

# Introduction

This is a lab guide on installing:
- OpenShift 4.14 into a disconnected network
And
- Standalone Quay
- Mirrored OpenShift registry
- IdM 
Into a connected network on top of KVM.

This also covers allowing connections between the two networks in KVM.

# Setup

For this lab, we will need 6 VMs. 3 RHEL 9 and 3 CoreOS which we will configure with kernel arguments. 

The overall architecture will look like this.

<TODO - Architecture diagram>

## KVM Networks

To start, we will need to configure the two networks in KVM and then allow traffic between them. 

### Infra Network

The Infra network will be a NAT network to allow it access to the internet. In this example, the network file will be called `infra-network.xml`

```xml
<TODO - copy the infra network xml here>
```

And to create the network

```bash
virsh net-create infra-network.xml
```

### OpenShift Network

To simulate a diconnected environment, we will be using a private KVM network for the OpenShift nodes. 

In this example, the network file will be called `ocp-network.xml`

```xml
<TODO - copy ocp network>
```

And to create the network

```bash
virsh net-create ocp-network.xml
```

### DNS and Proxy

Although the DNS has been configured internal to the KVM networks using dnsmasq, you will need to configure DNS rules and setup a proxy if we wish to use the OpenShift web console. If you just went to use the `oc` command line then you can use this from the `bastion.ocp.lan` node without any issues.

#### Tinyproxy

For web console ingress to the OpenShift cluster, the simplest way is to use tinyproxy on the KVM host. Ensure there are hosts file entries or DNS for the web console and oauth endpoints (see [DNS entries](#dns-entries) ) resolvable from the KVM host.

#### DNS Entries

Adding these entries to your lab DNS will allow resolution of the OpenShift web console and command line endpoints from external to the KVM networks.
- api.ocp.lan
- *.apps.ocp.lan

#### Host File

If DNS is not an option, the host file entries are possible. These are the core endpoints you will need to add to your hosts file:>
- <TODO - generate list of endpoints>

### Allow Traffic Between KVM Networks

To allow traffic from the OpenShift network to reach the infrastructure network, you will need to add some iptables. This will then pass traffic between the private KVM network and the KVM NAT network. This would also work between two private KVM networks.

```bash
<TODO - iptables net and masq rules>
```

## Infra VMs

In this section we will configure the two infrastructre VMs, one for running Quay and the other for IdM. 