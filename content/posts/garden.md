---
title: Garden Project
date: 2022-06-06
description: Garden project so far. There is more to do, but the main part is done.
tags:
  - Project
  - DIY
  - gallery
  - compare
author: Nick Campion
cover: https://s3.devurandom.io/devurandom/img/garden/garden-after-1.jpeg
toc: false
---

## Comparison

{{< figure src="https://s3.devurandom.io/devurandom/img/garden/garden1-COLLAGE.jpg" alt="Garden Corner 1" position="center" style="border-radius: 8px;" caption="Back Right Corner" captionPosition="center" >}}
{{< figure src="https://s3.devurandom.io/devurandom/img/garden/garden2-COLLAGE.jpg" alt="Garden Corner 2" position="center" style="border-radius: 8px;" caption="House Left Corner" captionPosition="center" >}}
{{< figure src="https://s3.devurandom.io/devurandom/img/garden/garden3-COLLAGE.jpg" alt="Garden Corner 3" position="center" style="border-radius: 8px;" caption="House Right Corner" captionPosition="center" >}}
{{< figure src="https://s3.devurandom.io/devurandom/img/garden/garden4-COLLAGE.jpg" alt="Garden Corner 4" position="center" style="border-radius: 8px;" caption="Back Left Corner" captionPosition="center" >}}

## Gallery

This is stages along the way.

{{< gallery name="Garden" title="Garden" >}}

[img01.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img01.jpg)
[img02.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img02.jpg)
[img03.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img03.jpg)
[img04.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img04.jpg)
[img05.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img05.jpg)
[img06.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img06.jpg)
[img07.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img07.jpg)  
[img08.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img08.jpg)
[img09.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img09.jpg)
[img10.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img10.jpg)
[img11.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img11.jpg)
[img12.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img12.jpg)
[img13.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img13.jpg)
[img14.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img14.jpg)
[img15.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img15.jpg)
[img16.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img16.jpg)
[img17.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img17.jpg)
[img18.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img18.jpg)
[img19.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img19.jpg)
[img20.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img20.jpg)
[img21.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img21.jpg)
[img22.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img22.jpg)
[img23.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img23.jpg)
[img24.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img24.jpg)
[img25.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img25.jpg)
[img26.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img26.jpg)
[img27.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img27.jpg)
[img28.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img28.jpg)
[img29.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img29.jpg)
[img30.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img30.jpg)
[img31.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img31.jpg)
[img32.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img32.jpg)
[img33.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img33.jpg)
[img34.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img34.jpg)
[img35.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img35.jpg)
[img36.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img36.jpg)
[img37.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img37.jpg)
[img38.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img38.jpg)
[img39.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img39.jpg)
[vid01.mp4](https://s3.devurandom.io/devurandom/img/garden/stages/vid01.mp4)
[img40.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img40.jpg)
[img41.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img41.jpg)
[img42.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img42.jpg)
[img43.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img43.jpg)
[img44.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img44.jpg)
[img45.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img45.jpg)
[img46.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img46.jpg)
[img47.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img47.jpg)
[img48.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img48.jpg)
[img49.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img49.jpg)
i[img50.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img50.jpg)
[img51.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img51.jpg)
[img52.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img52.jpg)
[img53.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img53.jpg)
[img54.jpg](https://s3.devurandom.io/devurandom/img/garden/stages/img54.jpg)

{{< /gallery >}}
