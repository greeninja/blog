---
title: Hugo Comments with Utterance
date: 2021-11-19
description: Setup Hugo post comments, using Utterance
tags:
  - Hugo
  - Utterance
author: Nick Campion
toc: false
cover: https://s3.devurandom.io/devurandom/img/utterances/utterances-with-logo.png
draft: false
---

## Summary

Adding comments to a Hugo blog is simple if your using the build in [Disqus](http://disqus.com/) template described [in the Hugo docs](https://gohugo.io/content-management/comments/).

But, if you don't need something that big, why not just use GitHub issues for your comments? This is where [Utterances](https://utteranc.es) comes in really handy and with a simple set up too.

## Setup

- If your not using [GitHub](https://github.com) for your blog already, you will need to set up a new repo for the comments. Ensure that the issues feature is enabled.

- Once you have a repo set up, you will need to install the Utterances app. Clicking [this link](https://github.com/apps/utterances) will start the setup for you.

- In your `config.toml` add your repo and which theme you would like.

{{< code language="ini" expand="Show" collapse="Hide" isCollapsed="false" >}}
[params]
[params.utterances]
repo="<owner>/<repo>" # eg: greeninja/devurandom-comments
theme="github-dark"
{{< /code >}}

- Add this snippet into your `layouts/partials/comments.html` or where ever you want the comments section to be. This could also be added as a partial if you preferred.

{{< code language="html" expand="Show" collapse="Hide" isCollapsed="false" >}}

<div class="utterances">
  <script
    src="https://utteranc.es/client.js"
    repo="{{ $.Site.Params.utterances.repo }}"
    issue-term="pathname"
    theme="{{ $.Site.Params.utterances.theme }}"
    crossorigin="anonymous"
    async
  ></script>
</div>
{{< /code >}}

## Result

Now you should have comments enabled on your site, backed by GitHub issues.

{{< figure src="https://s3.devurandom.io/devurandom/img/utterances/utterances.png" alt="Utterance Comments" position="center" style="border-radius: 8px;" caption="Utterance Comments" captionPosition="center" >}}
