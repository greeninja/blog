---
title: IdM Ansible Lab
date: 2020-07-27
description: WIP IdM / FreeIPA lab using Ansible to set it up. VM's built with Satalite / Katello
draft: true
tags:
  - IdM
  - FreeIPA
  - Ansible
  - Labs
---

{{< notice info >}}
WIP
{{< /notice >}}

The plan is to get 3 FreeIPA servers replicting from one to the other.

{{< mermaid align="center" >}}
graph LR
    A(ipa1)
    B(ipa2)
    C(ipa3)

    A -- Replication --> B
    B -- Replication --> C
{{< /mermaid >}}

Then sign a client or two into the IPA cluster.

The ansible to set the servers up will be in [gitlab](https://gitlab.com/greeninja/idm-ansible)


