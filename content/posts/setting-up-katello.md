---
title: Setting up Katello
date: 2020-07-24
description: Setting up Katello on a single VM
tags:
  - Katello
  - Satalite
  - Labs
---

# Prereqs

Just following their [instruction](https://theforeman.org/plugins/katello/3.15/installation/index.html) on a CentOS 7 VM

VM specs:

* 200GB disk
* 2vCPU
* 8GB Mem
* Single NIC in the management network (for now)

{{< notice note >}}
As this is a lab machine, there isn't any dns for it, had to add the hostname to `/etc/hosts` for the installer to pass it's checks.
{{< /notice >}}

# Setup

This is where the huge array of Foreman options comes into play, as well as a few from the Katello plugin. 
In essance, I need this machine to be able to:
* Discover Hosts for Build
* Provision KVM machines on the lab machine
* Mirror RHEL & CentOS repo's for packages (the wifi on this lab is painfully slow)
* Provide DNS to all machines built
* Manage subnet DHCP
* Power control for the VMs

__Pre Installed packages:__
- vim
- lsof
- bind
- bind-utils
- dhcp

__Foreman Installer__

So the giant array of options for install I used are:

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
foreman-installer --scenario katello \
  --enable-foreman-compute-libvirt \
  --enable-foreman-plugin-discovery \
  --enable-foreman-plugin-ansible \
  --enable-foreman-cli-remote-execution \
  --enable-foreman-proxy-plugin-discovery \
  --foreman-proxy-dhcp=true \
  --foreman-proxy-dhcp-interface=eth0 \
  --foreman-proxy-dhcp-gateway=10.44.1.1 \ 
  --foreman-proxy-dhcp-nameservers=10.44.1.5 \
  --foreman-proxy-dns=true \
  --foreman-proxy-dns-interface=eth0 \
  --foreman-proxy-dns-zone=lab.local \
  --foreman-proxy-dns-reverse=1.44.10.in-addr.arpa \
  --foreman-proxy-dns-forwarders=8.8.8.8 \
  --foreman-proxy-tftp=true \
  --foreman-proxy-tftp-servername=10.44.1.5 \
  --foreman-proxy-dhcp-range="10.44.1.20 10.44.1.200"

{{< /code >}}

There are loads more and can be seen with `foreman-installer --scenario katello --help`

> From here on, things can be configured in the Foreman web GUI or with the hammer CLI.
