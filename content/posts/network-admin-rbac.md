---
title: Openshift RBAC for Network Admin
date: 2020-11-25
description: An RBAC that allows Network admins to manage Network Policy objects in a cluster.
tags:
  - Openshift
  - RBAC
author: Nick Campion
---

## Openshif RBAC for Network Admin

In order for a network admin to manage the NetworkPolicy objects in a cluster, they will need a set of extended rules. This ruleset will allow the network admin to see all the projects and namespaces, along with a view only on the pods and services within each projects to allow them to configure the appropriate rules.

A brief list of what the Network Admins will be able to do:

`list` and `get` on:
- Namespaces
- Projects
- Pods
- Services
- Endpoints
- Networkpolicies

`create`, `delete` and `patch` on:
- Networkpolicies

This is done by creating a new ClusterRole

First, create the network-admin ClusterRole yaml

{{< code language="yaml" collapse="Hide" isCollapsed="false" >}}
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  name: network-admin
rules:
- apiGroups:
  - ""
  resources:
  - namespaces
  verbs:
  - get
  - list
- apiGroups:
  - ""
  - project.openshift.io
  resources:
  - projects
  verbs:
  - get
  - list
- apiGroups:
  - ""
  resources:
  - endpoints
  - pods
  - services
  - services/status
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - networking.k8s.io
  resources:
  - networkpolicies
  verbs:
  - get
  - list
  - watch
  - edit
  - delete
  - patch
  - create
- apiGroups:
  - extensions
  resources:
  - networkpolicies
  verbs:
  - get
  - list
  - watch
  - edit
  - delete
  - patch
  - create
{{< /code >}}

Then create this ClusterRole

{{< code language="bash" collapse="Hide" isCollapsed="false" >}}
# oc create -f network-admin.yaml 
clusterrole.rbac.authorization.k8s.io/network-admin created
{{< /code >}}

Once this is created, Network Admin users can have this ClusterRole added to them

{{< code language="bash" collapse="Hide" isCollapsed="false" >}}
# oc adm policy add-cluster-role-to-user network-admin user2
clusterrole.rbac.authorization.k8s.io/network-admin added: "user2"
{{< /code >}}

Alternatively, if there is a group for the Network Admins, this ClusterRole can be applied to the group.

{{< code language="bash" collapse="Hide" isCollapsed="false" >}}
# oc adm policy add-cluster-role-to-group network-admin network-admins-group
clusterrole.rbac.authorization.k8s.io/network-admin added: "network-admins-group"
{{< /code >}}
