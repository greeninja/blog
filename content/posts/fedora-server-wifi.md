---
title: Fedora Server 32 and Wifi
date: 2020-07-24
description: Getting wifi to work on Fedora 32 server
tags: 
  - Fedora
author: Nick Campion
---

## Prefix

For starters, I have no idea why you would want to do Fedora server with _just_ a wifi device to connect it to the network, but for me space limitations for where the homelab can live, meant this was the route I ended up going down...

## The Problem

The wifi adapter couldn't be activated by NetworkManager:

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
$ nmcli d
DEVICE       TYPE      STATE        CONNECTION 
wlp0s29u1u6  wifi      unavailable  --
eno1         ethernet  unavailable  --         
lo           loopback  unmanaged    -- 
{{< /code >}}

Even `rfkill` wouldn't help it come back:

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}

$ rfkill unblock wifi

$ rfkill list
0: phy0: Wireless LAN
        Soft blocked: no
        Hard blocked: no
{{< /code >}}

Syslog was being less than helpful:

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
NetworkManager[3028]: <info>  [1595529900.1450] device (wlp0s29u1u6): set-hw-addr: set MAC address to 92:76:C5:83:F3:34 (scanning)
NetworkManager[3422]: <info>  [1595530262.3698] manager: (wlp0s29u1u6): new 802.11 Wi-Fi device (/org/freedesktop/NetworkManager/Devices/3)
NetworkManager[3422]: <info>  [1595530262.3710] device (wlp0s29u1u6): state change: unmanaged -> unavailable (reason 'managed', sys-iface-state: 'external')
{{< /code >}}

Yet `iwlist` would return all the local SSID networks. 

## The Solution

For me at least, the problem was a missing package allowing NetworkManaer to bring up the the interface ans manage it, namely the `wpa_supplicant` package.

Once installed on the server and NetworkManager restarted, the interface sprung up and could be configured using:

{{< code language="bash" expand="Show" collapse="Hide" isCollapsed="false" >}}
$  nmcli device wifi con "$SSID" password "$Password"
{{< /code >}}

All happy now.
