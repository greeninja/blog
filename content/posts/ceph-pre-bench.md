---
title: Ceph Pre Install Testing
date: 2020-07-31
description: WIP Before installing ceph, this should be able to check that nothing is going to catch you out.
draft: true
tags:
  - Ansible
  - Ceph
author: Nick Campion
---

{{< notice info >}}
WIP
{{< /notice >}}

This should be a full suite of tests that can be run before installing Ceph to ensure there will be nothing that jumps out as a surprise and compromises performance. It will probably not be comprehensive, but should be a good start.

[Gitlab repo](https://gitlab.com/greeninja/ceph-pre-install-bench)

# Tests

This needs to do the below tests:

- Network
  - Full mesh iperf (storage network)
  - Full mesh iperf (public network & at least one client)
  - Checking MTU is not an issue (how?)
- Disks
  - fio tests on each disk to be used in the cluster.
- Memory
  - sysbench
- CPU
  - sysbench
- Kernel
  - tuned-adm profile
  - patches applied
  - c state

# Reports

- Network throughput from each node (storage network)
- Network throughput from each node (public network)
- Fio test - split into harddrive type
- Memory test for each node
- CPU for each node
- Kernel test report
- Summary

# Example Outputs

 
