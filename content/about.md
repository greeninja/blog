---
title: About
date: 2020-07-21
author: Nick Campion
---

Currently, I work as an Infastructure and Cloud consultant at Red Hat within the consulting team. I predominantly work in the storage and container ecosystems but have an interest in most things Linux related.

Prior to this I worked in the hosting industry building solutions at a global scale.

The purpose of this site is for me to write about things I’ve found interesting, as well as a general scratch pad for labs and test.

Opinions expressed are my own and not related to/endorsed by my employer in any fashion.


## The Website

This is a static website stored entirely within [GitLab](https://gitlab.com). It use the [Hugo](https://gohugo.io/) static site generator to build the site.

The site is then hosted on [Netlify](https://netlify.com) on a global CDN.

Updates are done using the standard git workflow, then a trigger is sent from Gitlab to Netlify to rebuild and republish the site.

## Infrastructure

### Gitlab

The site is maintained in GitLab [here](https://gitlab.com/greeninja/blog).

For images and other large binary conetent, [GitLab LFS](https://docs.gitlab.com/ee/topics/git/lfs/) is utilised. This is to prevent the main repo filling up with (non-necessary) versioning information for these data-types.

### Hugo

When adding new content for the site, running a preview can be very helpful. This can be done by having the hugo binary on your local machine and running `hugo server -D` from within the root of the git directory. This will then have a development version of the site at [http://127.0.0.1:1313](http://127.0.0.1:1313). 

When using a local development hugo and Netlify, be sure to have the Hugo Version defined in a `netlify.toml` file to allow the site to build with the relevant version of Hugo.

The theme is a slightly butchered version of [panr's terminal theme](https://github.com/panr/hugo-theme-terminal)

### Netlify

[Netlify](https://www.netlify.com/) is a web developer platform that multiplies productivity.

By unifying the elements of the modern decoupled web, from local development to advanced edge logic, Netlify enables a 10x faster path to much more performant, secure, and scalable websites and apps.

Current build status from Netlify
[![Netlify Status](https://api.netlify.com/api/v1/badges/410113b2-df58-49d6-a89e-622a12fcc384/deploy-status)](https://app.netlify.com/sites/optimistic-stonebraker-a22d41/deploys)

### Workflow

Adding content to the site is done using this method:

- Checkout the GitLab repository
- Update or add content to `content/posts`
- Check the new content with `aspell` and the `--markdown` filter
{{< code language="bash" title="aspell example" id="1" expand="Show" collapse="Hide" isCollapsed="true" >}}
aspell --mode=markdown check gluster-3.8.4-upgrade.md`
{{< /code >}}
- Push new content back to GitLab
- NetLify triggers a build once git is updated - it checks out the content from git, applies the Hugo generator to the site and then publishes it on a specific URL
{{< code language="bash" title="Netlify Build" id="2" expand="Show" collapse="Hide" isCollapsed="true" >}}
11:32:36 AM: Build ready to start
11:32:38 AM: build-image version: 53b83b6bede2920f236b25b6f5a95334320dc849
11:32:38 AM: build-image tag: v3.6.0
11:32:38 AM: buildbot version: a706ec7a557bcc28584843816a376a10c08955ca
11:32:38 AM: Fetching cached dependencies
11:32:38 AM: Starting to download cache of 97.0MB
11:32:39 AM: Finished downloading cache in 655.401572ms
11:32:39 AM: Starting to extract cache
11:32:41 AM: Finished extracting cache in 2.536508163s
11:32:41 AM: Finished fetching cache in 3.2091451s
11:32:41 AM: Starting to prepare the repo for build
11:32:41 AM: Git LFS enabled
11:32:42 AM: Preparing Git Reference refs/heads/master
11:32:44 AM: using git lfs fetchinclude override of: *
11:32:45 AM: Different build command detected, going to use the one specified in the Netlify configuration file: 'hugo --gc --minify' versus 'hugo' in the Netlify UI
11:32:45 AM: Starting build script
11:32:45 AM: Installing dependencies
11:32:45 AM: Python version set to 2.7
11:32:45 AM: Started restoring cached node version
11:32:47 AM: Finished restoring cached node version
11:32:48 AM: v12.18.0 is already installed.
11:32:49 AM: Now using node v12.18.0 (npm v6.14.4)
11:32:49 AM: Started restoring cached build plugins
11:32:49 AM: Finished restoring cached build plugins
11:32:49 AM: Attempting ruby version 2.7.1, read from environment
11:32:50 AM: Using ruby version 2.7.1
11:32:50 AM: Using PHP version 5.6
11:32:50 AM: Installing Hugo 0.70.0
11:32:50 AM: Hugo Static Site Generator v0.70.0-7F47B99E/extended linux/amd64 BuildDate: 2020-05-06T11:26:13Z
11:32:50 AM: Started restoring cached go cache
11:32:50 AM: Finished restoring cached go cache
11:32:50 AM: go version go1.14.4 linux/amd64
11:32:50 AM: go version go1.14.4 linux/amd64
11:32:50 AM: Installing missing commands
11:32:50 AM: Verify run directory
11:32:52 AM: ​
11:32:52 AM: ────────────────────────────────────────────────────────────────
11:32:52 AM:   Netlify Build                                                 
11:32:52 AM: ────────────────────────────────────────────────────────────────
11:32:52 AM: ​
11:32:52 AM: ❯ Version
11:32:52 AM:   @netlify/build 8.0.3
11:32:52 AM: ​
11:32:52 AM: ❯ Flags
11:32:52 AM:   deployId: 5ffeda54dcc39f0008f2544c
11:32:52 AM:   mode: buildbot
11:32:52 AM: ​
11:32:52 AM: ❯ Current directory
11:32:52 AM:   /opt/build/repo
11:32:52 AM: ​
11:32:52 AM: ❯ Config file
11:32:52 AM:   /opt/build/repo/netlify.toml
11:32:52 AM: ​
11:32:52 AM: ❯ Context
11:32:52 AM:   production
11:32:52 AM: ​
11:32:52 AM: ────────────────────────────────────────────────────────────────
11:32:52 AM:   1. build.command from netlify.toml                            
11:32:52 AM: ────────────────────────────────────────────────────────────────
11:32:52 AM: ​
11:32:52 AM: $ hugo --gc --minify
11:32:52 AM: Building sites … WARNING: calling IsSet with unsupported type "invalid" (<nil>) will always return false.
11:32:52 AM:                    | EN
11:32:52 AM: -------------------+-----
11:32:52 AM:   Pages            | 49
11:32:52 AM:   Paginator pages  |  0
11:32:52 AM:   Non-page files   |  0
11:32:52 AM:   Static files     | 19
11:32:52 AM:   Processed images |  0
11:32:52 AM:   Aliases          | 16
11:32:52 AM:   Sitemaps         |  1
11:32:52 AM:   Cleaned          |  0
11:32:52 AM: Total in 147 ms
11:32:52 AM: ​
11:32:52 AM: (build.command completed in 199ms)
11:32:52 AM: ​
11:32:52 AM: ────────────────────────────────────────────────────────────────
11:32:52 AM:   Netlify Build Complete                                        
11:32:52 AM: ────────────────────────────────────────────────────────────────
11:32:52 AM: ​
11:32:52 AM: (Netlify Build completed in 259ms)
11:32:52 AM: Caching artifacts
11:32:52 AM: Started saving build plugins
11:32:52 AM: Finished saving build plugins
11:32:52 AM: Started saving pip cache
11:32:52 AM: Finished saving pip cache
11:32:52 AM: Started saving emacs cask dependencies
11:32:52 AM: Finished saving emacs cask dependencies
11:32:52 AM: Started saving maven dependencies
11:32:52 AM: Finished saving maven dependencies
11:32:52 AM: Started saving boot dependencies
11:32:52 AM: Finished saving boot dependencies
11:32:52 AM: Started saving rust rustup cache
11:32:52 AM: Finished saving rust rustup cache
11:32:52 AM: Started saving rust cargo bin cache
11:32:52 AM: Finished saving rust cargo bin cache
11:32:52 AM: Started saving go dependencies
11:32:52 AM: Finished saving go dependencies
11:32:52 AM: Build script success
11:32:52 AM: Starting to deploy site from 'public'
11:32:52 AM: Creating deploy tree 
11:32:53 AM: Creating deploy upload records
11:32:53 AM: 47 new files to upload
11:32:53 AM: 0 new functions to upload
11:32:54 AM: Starting post processing
11:32:54 AM: Post processing - HTML
11:32:57 AM: Post processing - header rules
11:32:57 AM: Post processing - redirect rules
11:32:57 AM: Post processing done
11:32:57 AM: Site is live ✨
11:33:09 AM: Finished processing build request in 31.475283799s
{{< /code >}}

